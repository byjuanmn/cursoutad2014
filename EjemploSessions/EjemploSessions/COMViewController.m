//
//  COMViewController.m
//  EjemploSessions
//
//  Created by Juan Antonio Martin Noguera on 04/04/14.
//  Copyright (c) 2014 cloudonmobile. All rights reserved.
//

#import "COMViewController.h"

@interface COMViewController() <NSURLSessionDownloadDelegate, NSURLSessionDelegate> {}

@end

@implementation COMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession * session = [NSURLSession sessionWithConfiguration:conf delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"http://www.nasa.gov/images/content/618486main_earth_full.jpg"];
    NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithURL:url];
    [downloadTask resume];
    
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    
    NSData *data = [NSData dataWithContentsOfURL:location];
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
        [self.progress setHidden:YES];
        self.imageView.image = [UIImage imageWithData:data];
    });
    
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes{
    
}
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    
    float progress = (double)totalBytesWritten / (double) totalBytesExpectedToWrite;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.progress setProgress:progress];
        NSLog(@"%.2f", progress);
    });
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}













@end
