//
//  COMViewController.h
//  EjemploSessions
//
//  Created by Juan Antonio Martin Noguera on 04/04/14.
//  Copyright (c) 2014 cloudonmobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface COMViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIProgressView *progress;

@end
