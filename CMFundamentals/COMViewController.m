//
//  COMViewController.m
//  CMFundamentals
//
//  Created by Juan Antonio Martin Noguera on 03/04/14.
//  Copyright (c) 2014 cloudonmobile. All rights reserved.
//

#import "COMViewController.h"
@import CoreMotion;

@interface COMViewController ()

@end

@implementation COMViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    CMMotionManager *motionManager = [[CMMotionManager alloc]init];
    
    [motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
        [self outputAccelerometer:accelerometerData.acceleration];
    }];
    
    [motionManager startGyroUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMGyroData *gyroData, NSError *error) {
        [self outputGyroscope:gyroData.rotationRate];
    }];
}

-(void)outputAccelerometer:(CMAcceleration)acceleration{
    self.acelX.text = [NSString stringWithFormat:@"%.2fg", acceleration.x];
    if (fabs(acceleration.x) > fabs(mAcelX)) {
        mAcelX = acceleration.x;
    }
    
    self.acelY.text = [NSString stringWithFormat:@"%.2fg", acceleration.y];
    if (fabs(acceleration.y) > fabs(mAcelY)) {
        mAcelY = acceleration.y;
    }
    
    self.acelZ.text = [NSString stringWithFormat:@"%.2fg", acceleration.z];
    if (fabs(acceleration.z) > fabs(mAcelZ)) {
        mAcelZ = acceleration.z;
    }
    self.maxAcelX.text = [NSString stringWithFormat:@"%.2f", mAcelX];
    self.maxAcelY.text = [NSString stringWithFormat:@"%.2f", mAcelY];
    self.maxAcelZ.text = [NSString stringWithFormat:@"%.2f", mAcelZ];
    
}
-(void)outputGyroscope:(CMRotationRate)rotation{
    self.rotX.text = [NSString stringWithFormat:@"%.2f", rotation.x];
    if (fabs(rotation.x) > fabs(mRotX)) {
        mRotX = rotation.x;
    }
    self.rotY.text = [NSString stringWithFormat:@"%.2f", rotation.y];
    if (fabs(rotation.y) > fabs(mRotY)) {
        mRotY = rotation.y;
    }
    self.rotZ.text = [NSString stringWithFormat:@"%.2f", rotation.z];
    if (fabs(rotation.z) > fabs(mRotZ)) {
        mRotZ = rotation.z;
    }
    self.maxRotX.text = [NSString stringWithFormat:@"%.2f", mRotX];
    self.maxRotY.text = [NSString stringWithFormat:@"%.2f", mRotY];
    self.maxRotZ.text = [NSString stringWithFormat:@"%.2f", mRotZ];

    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)resetAcumuladores:(id)sender {
    
    mAcelX = 0;
    mAcelY = 0;
    mAcelZ = 0;
    mRotX = 0;
    mRotY = 0;
    mRotZ = 0;
}
@end
