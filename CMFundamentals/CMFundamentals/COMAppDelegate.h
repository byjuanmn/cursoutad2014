//
//  COMAppDelegate.h
//  CMFundamentals
//
//  Created by Juan Antonio Martin Noguera on 03/04/14.
//  Copyright (c) 2014 cloudonmobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface COMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
