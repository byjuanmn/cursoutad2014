//
//  main.m
//  CMFundamentals
//
//  Created by Juan Antonio Martin Noguera on 03/04/14.
//  Copyright (c) 2014 cloudonmobile. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "COMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([COMAppDelegate class]));
    }
}
