//
//  COMViewController.h
//  CMFundamentals
//
//  Created by Juan Antonio Martin Noguera on 03/04/14.
//  Copyright (c) 2014 cloudonmobile. All rights reserved.
//

#import <UIKit/UIKit.h>

double mAcelX;
double mAcelY;
double mAcelZ;
double mRotX;
double mRotY;
double mRotZ;

@interface COMViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *acelX;
@property (weak, nonatomic) IBOutlet UILabel *acelY;
@property (weak, nonatomic) IBOutlet UILabel *acelZ;
@property (weak, nonatomic) IBOutlet UILabel *rotX;
@property (weak, nonatomic) IBOutlet UILabel *rotY;
@property (weak, nonatomic) IBOutlet UILabel *rotZ;
@property (weak, nonatomic) IBOutlet UILabel *maxAcelX;
@property (weak, nonatomic) IBOutlet UILabel *maxAcelY;
@property (weak, nonatomic) IBOutlet UILabel *maxAcelZ;
@property (weak, nonatomic) IBOutlet UILabel *maxRotX;
@property (weak, nonatomic) IBOutlet UILabel *maxRotY;
@property (weak, nonatomic) IBOutlet UILabel *maxRotZ;

- (IBAction)resetAcumuladores:(id)sender;












@end
